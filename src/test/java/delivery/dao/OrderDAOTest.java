package delivery.dao;

import delivery.application.ServletInitializer;
import delivery.configuration.DataSourceConfig;
import delivery.log.LogWriter;
import delivery.model.Order;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ServletInitializer.class, DataSourceConfig.class})
@ActiveProfiles("test")
public class OrderDAOTest
{
    @Autowired
    @Qualifier("dataSource")
    DataSource dataSource;

    OrderDAO orders;


    @Before
    public void setUp()
    {
        orders = new OrderDAO(new JdbcTemplate(dataSource), new LogWriter());
    }

    /**
     * Проверяется, что при вызове метода {@link OrderDAO#get(long)}
     * c параметром один. запись будет найдена и метод вернёт {@link Optional<Order>}
     * и метод isPresent вернёт true
     */
    @Test
    public void getIsPresent()
    {
        Optional<Order> orderOptional = orders.get(1);
        assertTrue(orderOptional.isPresent());
    }

    @Test
    public void getAll()
    {
    }

    @Test
    public void testGetAll()
    {
    }

    @Test
    public void save()
    {

    }

    @Test
    public void update()
    {
    }
}