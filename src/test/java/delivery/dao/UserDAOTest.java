package delivery.dao;

import delivery.application.ServletInitializer;
import delivery.configuration.DataSourceConfig;
import delivery.model.Role;
import delivery.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ServletInitializer.class, DataSourceConfig.class})
@ActiveProfiles("test")
public class UserDAOTest
{

    @Autowired
    @Qualifier("dataSource")
    DataSource dataSource;

    UserDAO users;

    User testUser = new User(
            1
            , "PPivanov"
            , "123"
            , "PPivanov@abyss.com"
            ,  Role.ADMINISTRATOR
    );

    @Before
    public void setUp()
    {
        users = new UserDAO(new JdbcTemplate(dataSource));

    }

    @Test
    public void getByLoginPresent()
    {
        Optional<User> user =  users.get(testUser.getLogin());

        Assertions.assertTrue(user.isPresent());
    }

    @Test
    public void getByLoginEquals()
    {

        Optional<User> user =  users.get(testUser.getLogin());

        assertEquals(testUser, user.get());
    }

}