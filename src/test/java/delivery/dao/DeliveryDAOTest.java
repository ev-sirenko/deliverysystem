package delivery.dao;

import delivery.application.ServletInitializer;
import delivery.configuration.DataSourceConfig;
import delivery.model.Delivery;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ServletInitializer.class, DataSourceConfig.class})
@ActiveProfiles("test")
class DeliveryDAOTest
{
    @Autowired
    @Qualifier("dataSource")
    DataSource dataSource;

    DeliveryDao deliveryDao;

    /**
     * Тестируем метод {@link DeliveryDao#getByUserId(long)}
     *
     * @throws SQLException
     * @throws IOException
     */
    @Test
    void getByUserIdTest() {
        deliveryDao = new DeliveryDao(new JdbcTemplate(dataSource));
        Assertions.assertEquals(deliveryDao.getByUserId(2).size(), 2);
    }

    /**
     * Тестируем метод {@link DeliveryDao#getAll()}
     *
     * @throws SQLException
     * @throws IOException
     */
    @Test
    void getAllTest() throws SQLException, IOException {
        deliveryDao = new DeliveryDao(new JdbcTemplate(dataSource));
        Assertions.assertEquals(deliveryDao.getAll().size(), 3);
    }



    /**
     * Тестируем метод {@link DeliveryDao#save(Delivery)}
     *
     * @throws SQLException
     * @throws IOException
     */
    @Test
    void getSaveTest() throws SQLException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        Delivery delivery = new Delivery( 2, 2, LocalDateTime.parse("2022-02-02 13:00:00", formatter),  LocalDateTime.parse("2023-03-03 12:00:00", formatter));
        deliveryDao = new DeliveryDao(new JdbcTemplate(dataSource));
        deliveryDao.save(delivery).get();

        Delivery deliveryFromDB = deliveryDao.get(2,2).get();

        Assertions.assertEquals( deliveryFromDB.getIdOrder(), 2);
        Assertions.assertEquals( deliveryFromDB.getIdUser(), 2);
        Assertions.assertEquals(deliveryFromDB.getDateDeliveredStart().format(formatter), "2022-02-02 13:00:00");
        Assertions.assertEquals( deliveryFromDB.getDateDeliveredEnd().format(formatter), "2023-03-03 12:00:00");

    }

    @Test
    void getIdTest() {
        deliveryDao = new DeliveryDao(new JdbcTemplate(dataSource));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        Delivery delivery = deliveryDao.get(1,1).get();

        Assertions.assertEquals( delivery.getIdOrder(), 1);
        Assertions.assertEquals( delivery.getIdUser(), 1);
        Assertions.assertEquals(delivery.getDateDeliveredStart().format(formatter), "2022-03-08 12:53:13");
        Assertions.assertEquals( delivery.getDateDeliveredEnd().format(formatter), "2022-03-08 15:53:13");
    }

    /**
     * Тестируем метод {@link DeliveryDao#update(Delivery)}
     *
     * @throws SQLException
     * @throws IOException
     */
    @Test
    void getUpdateTest() throws SQLException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        Delivery delivery = new Delivery( 3, 2, null, LocalDateTime.now());

        deliveryDao = new DeliveryDao(new JdbcTemplate(dataSource));

        deliveryDao.update(delivery);
        Delivery deliveryFromDB = deliveryDao.get(3,2).get();
        Assertions.assertEquals(deliveryFromDB.getIdOrder(), 3);
        Assertions.assertEquals(deliveryFromDB.getIdUser(), 2);
        Assertions.assertEquals(deliveryFromDB.getDateDeliveredStart().format(formatter), "2022-02-08 12:54:13");
        Assertions.assertEquals(deliveryFromDB.getDateDeliveredEnd().format(formatter), delivery.getDateDeliveredEnd().format(formatter));
    }


}