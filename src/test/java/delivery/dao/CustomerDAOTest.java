package delivery.dao;

import delivery.application.ServletInitializer;
import delivery.configuration.DataSourceConfig;

import delivery.model.Address;
import delivery.model.Customer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ServletInitializer.class, DataSourceConfig.class})
@ActiveProfiles("test")
class CustomerDAOTest {
    @Autowired
    @Qualifier("dataSource")
    DataSource dataSource;

    @Autowired
    CustomerDAO customerDAO;

    List<Customer> customerList;
    List<Address> addressList;

    @BeforeEach
    void start() {

        customerDAO = new CustomerDAO(new JdbcTemplate(dataSource));
        addressList = new ArrayList<>();
        addressList.add(new Address(1, "Street", "4", "54", 4));
        addressList.add(new Address(2, "Street2", "42", "542", 42));
        customerList = new ArrayList<>();
        customerList.add(new Customer("89895662323"));
        customerList.add(new Customer("89895662343"));
    }

    /**
     * @see CustomerDAO#getAll()
     */
    @Test
    void getAll() {
        Assertions.assertEquals(customerDAO.getAll().toString(), customerList.toString());
    }

    /**
     * @see CustomerDAO#get(String)
     */
    @Test
    void getPhone() {
        String phone = "89895662323";
        Customer customer = new Customer(phone);
        Assertions.assertEquals(customerDAO.get(phone).get().toString(), customer.toString());
    }

    /**
     * @see CustomerDAO#get(long)
     */
    @Test
    void getId() {
        Address address = new Address(2, "Street2", "42", "542", 42);
        Assertions.assertEquals(customerDAO.get(2).get().toString(), address.toString());
    }

    /**
     * @see CustomerDAO#getAll(Object)
     */
    @Test
    void getAllAddressPhone() {
        Customer customer = new Customer("89895662343");
        List<Address> list = customerDAO.getAll(customer);
        Address address = new Address(2, "Street2", "42", "542", 42);
        Assertions.assertEquals(list.get(0).toString(), address.toString());
    }

    /**
     * @see CustomerDAO#getAllAddress()
     */
    @Test
    void getAllAddress() {

        List<Address> list = customerDAO.getAllAddress();
        Assertions.assertEquals(list.size(), addressList.size());
    }

    /**
     * @see CustomerDAO#save(Object)
     */
    @Test
    void save() {
        Address address = new Address(3, "Street3", "43", "543", 43);
        customerDAO.save(address);
        addressList.add(address);
        Assertions.assertEquals(customerDAO.getAllAddress().size(), addressList.size());

    }

    /**
     * @see CustomerDAO#updateAddressWithPhone(Customer, Address)
     */
    @Test
    void updateAddressWithPhone() {

        Address address = new Address(1, "Street3", "43", "543", 43);
        Customer customer = new Customer("89895662323");
        customerDAO.updateAddressWithPhone(customer, address);
        Assertions.assertEquals(customerDAO.getAll(customer).get(0).toString()
                , address.toString());

    }

    /**
     * @see CustomerDAO#update(Object)
     */
    @Test
    void update() throws SQLException {

        Address address = new Address(1, "Street4", "44", "544", 44);
        addressList.set(0, address);
        Assertions.assertEquals(customerDAO.update(address).get().toString()
                , address.toString());

    }
}