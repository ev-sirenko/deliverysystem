package delivery.mapper;

import delivery.model.CustomerAddress;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerAddressRowMapper implements RowMapper {
    @Override
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        CustomerAddress customerAddress = new CustomerAddress(
                rs.getString("phone"),
                rs.getLong("id_address")
        );
        return customerAddress;
    }
}
