package delivery.mapper;

import delivery.model.Status;
import delivery.model.StatusHistory;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class StatusHistoryMapper implements RowMapper<StatusHistory>
{
    @Override
    public StatusHistory mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        StatusHistory history = new StatusHistory();
        history.setOrder(new OrderRowMapper().mapRow(rs, rowNum));
        history.setStatus(Status.getStatusByCode(rs.getLong("history_status")));
        history.setDate_set(LocalDateTime.parse(rs.getString("date_set"), formatter));

        return history;
    }
}
