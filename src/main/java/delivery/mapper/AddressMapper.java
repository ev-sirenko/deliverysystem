package delivery.mapper;

import delivery.model.Address;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AddressMapper implements RowMapper {

    @Override
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        Address address = new Address(
                rs.getLong("id_address"),
                rs.getString("street"),
                rs.getString("building"),
                rs.getString("room"),
                rs.getInt("building_floor")
        );

        return address;
    }
}

