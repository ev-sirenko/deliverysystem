package delivery.mapper;

import delivery.model.Address;
import delivery.model.Order;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class OrderRowMapper implements RowMapper<Order>
{
    @Override
    public Order mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        Order order = new Order();
        order.setId(rs.getLong("ID_ORDER"));
        order.setCustomer(new CustomerRowMapper().mapRow(rs, rowNum));
        order.setCreated(LocalDateTime.parse(rs.getString("CREATED_TIME"), formatter));
        order.setLastModified(LocalDateTime.parse(rs.getString("LAST_MODIFIED_TIME"), formatter));
        order.setStatus(new StatusMapper().mapRow(rs, rowNum));
        order.setStartDate(LocalDate.parse(rs.getString("START_DATE")
                , DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        order.setEndDate(LocalDate.parse(rs.getString("end_date")
                , DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        order.setDeliveryDesiredTime(rs.getString("what_time_to_deliver"));
        order.setAddress((Address) new AddressMapper().mapRow(rs, rowNum));

        return order;
    }
}
