package delivery.mapper;

import delivery.model.Delivery;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DeliveryRowMapper implements RowMapper<Delivery> {
    /**
     * Происходит построчная {@link RowMapper} обработка результирующего набора данных {@link ResultSet}
     * как результат запроса, с дальнейшим созданием и заполнением полей объекта класса {@link Delivery}
     *
     * @param rs набор данных
     * @param rowNum
     * @return объект класса {@link Delivery}
     * @throws SQLException
     */

    @Override
        public Delivery mapRow(ResultSet rs, int rowNum) throws SQLException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        Delivery delivery = new Delivery();
        delivery.setIdOrder(rs.getInt("id_order"));
        delivery.setIdUser(rs.getInt("id_user"));
        delivery.setDateDeliveredStart("".equals(rs.getString("start_date"))? null : LocalDateTime.parse(rs.getString("start_date"), formatter));
        delivery.setDateDeliveredEnd("".equals(rs.getString("end_date"))? null :LocalDateTime.parse(rs.getString("end_date"), formatter));
        return delivery;
    }
}
