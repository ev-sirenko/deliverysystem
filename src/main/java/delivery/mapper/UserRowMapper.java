package delivery.mapper;

import delivery.model.Role;
import delivery.model.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper<User>
{
    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        User user = new User();

        user.setId(rs.getLong("ID_USER"));
        user.setLogin(rs.getString("LOGIN"));
        user.setPassword(rs.getString("PASSWORD"));
        user.setEmail(rs.getString("email"));
        user.setRole(Role.getRoleByCode(rs.getLong("USER_ROLE")));

        return user;
    }
}
