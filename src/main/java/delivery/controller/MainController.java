package delivery.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/")
public class MainController
{
    /**
     * При переходе по url / вызывается index.html
     * @return объект {@link ModelAndView}
     */
    @GetMapping()
    public ModelAndView getIndex()
    {
        ModelAndView model = new ModelAndView();
        model.setViewName("index");
        return model;
    }

    /**
     * При переходе по url /index вызывается index.html
     * Для правила successfulLogoutURL
     * @return объект {@link ModelAndView}
     */
    @GetMapping(value = "/index")
    public ModelAndView getSuccessLogin()
    {
        ModelAndView model = new ModelAndView();
        model.setViewName("index");
        return model;
    }


    /**
     * При переходе по url /login вызывается login.html
     * @return объект {@link ModelAndView}
     */
    @GetMapping(value = "/login")
    public ModelAndView getCustomerLogin()
    {
        ModelAndView model = new ModelAndView();
        model.setViewName("login");
        return model;
    }


}
