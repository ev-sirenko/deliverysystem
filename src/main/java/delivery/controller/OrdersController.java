package delivery.controller;

import delivery.dao.*;
import delivery.exceptions.InvalidOrderStartDate;
import delivery.model.*;
import delivery.service.IOrderService;
import delivery.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@RestController
@RequestMapping("/orders")
public class OrdersController
{
    final MessageSource messages;

    final IOrderService<Order> orderService;

    final IUserService<User, Customer> userService;

    final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Autowired
    StatusHistoryDAO historyDao;

    @Autowired
    CustomerDAO customerDAO;

    @Autowired
    DeliveryDao deliveryDao;

    @Autowired
    OrderDAO orderDAO;

    public OrdersController(MessageSource messages
            , IOrderService<Order> orderService
            , IUserService<User
            , Customer> userService)
    {
        this.messages = messages;
        this.orderService = orderService;
        this.userService = userService;
    }

    /**
     * При переходе по url /orders/all вызывается all.html
     * @return объект {@link ModelAndView}
     */
    @GetMapping(value = "/all")
    public ModelAndView getOrderList(@RequestParam(required = false) String filter
            , @RequestParam Optional<String> filterStatus)
    {
        final ModelAndView model = new ModelAndView();
        final Map<String, String> filterMap = new HashMap<>();
        final LocalDate dateFromFilter = filter == null ? LocalDate.now() : LocalDate.parse(filter, dateFormat);

        filterMap.put("start_date", dateFromFilter.toString());
        model.addObject("filter", dateFromFilter);

        if (filterStatus.isPresent())
        {
            filterMap.put("status", filterStatus.get());
        }

        model.addObject("orders", orderService.getAll(filterMap));
        model.addObject("statusList", Status.getCodeDescription());

        model.setViewName("all");
        return model;
    }

    /**
     * При переходе по url /orders/courierAll.html вызывается courierAll.html
     * Список курьеров
     * @return объект {@link ModelAndView}
     */
    @GetMapping(value = "/courierAll")
    public ModelAndView getCourierList()
    {
        ModelAndView model = new ModelAndView();
        model.addObject("orders", orderService.getAll());
        model.addObject("couriers", userService.couriersList());
        model.setViewName("courierAll");
        return model;
    }
    /**
     * При переходе по url courier/deliverylist/info вызывается orderDetailsForDelivery.html
     * @return объект {@link ModelAndView}
     */
    @GetMapping(value="/deliveryinfo")
    public @ResponseBody
    ModelAndView getDeliver(@RequestParam(value = "idO")  Optional<String> idO, @RequestParam(value = "idU")  Optional<String> idU) {


        try {
            ModelAndView modelView = new ModelAndView();
            long idOr = Long.parseLong(idO.get());
            long idUs = Long.parseLong(idU.get());

            if (idOr == 0){
                modelView.setStatus(HttpStatus.NOT_FOUND);
                return modelView;
            }
            Optional<Delivery> deliveryOptional = deliveryDao.get(idOr,idUs);
            Optional<Order> order = orderDAO.get(deliveryOptional.get().getIdOrder());
            List<StatusHistory> history = historyDao.getAllByOrder(deliveryOptional.get().getIdOrder());

            if(order.isPresent()){
                modelView.setStatus(HttpStatus.OK);
                modelView.setViewName("deliveryinfo");
                modelView.addObject("order", order.get());
                modelView.addObject("deliveries", deliveryOptional.get());
                modelView.addObject("statusHistory", history);
                modelView.addObject("statusList", Status.getCodeDescription());
            }
            return modelView;
        } catch (NumberFormatException ex) {
            throw new NumberFormatException();
        }
    }

    /**
     * Список активных заказов курьера
     * @return {@link ModelAndView} с представлением courierAll и
     */
    @GetMapping(value="/activdeliverylist")
    public @ResponseBody
    ModelAndView getCourierOrders(@RequestParam(value = "idO")  Optional<String> id_courier)
    {
        long idUs =  Long.parseLong(id_courier.get());;
        ModelAndView model = new ModelAndView();
        model.addObject("deliveries", deliveryDao.getByUserId(idUs));

        model.setViewName("activdeliverylist");
        return model;
    }

    /**
     * При получении GET запроса с параметром id, метод должен вернуть объект {@link ModelAndView}
     * @param id_order - id заказа в БД
     * @return - страница детализации заказа
     */
    @GetMapping(value="/{id}")
    public @ResponseBody
    ModelAndView getOrder(@PathVariable("id") Optional<String> id_order)
    {
        try
        {
            ModelAndView modelView = new ModelAndView();
            if (!id_order.isPresent())
            {
                modelView.setStatus(HttpStatus.NOT_FOUND);
                return modelView;
            }
            long id = Long.parseLong(id_order.get());

            List<StatusHistory> history = historyDao.getAllByOrder(id);

            modelView.setStatus(HttpStatus.OK);
            modelView.setViewName("orderDetails");
            modelView.addObject("order", orderService.get(id));
            modelView.addObject("statusHistory", history);
            modelView.addObject("statusList", Status.getCodeDescription());
            modelView.addObject("couriersList", userService.couriersList());
            return modelView;
        }
        catch (NumberFormatException ex)
        {
            throw new NumberFormatException();
        }
    }

    @PostMapping(value="{id}")
    public @ResponseBody
    ModelAndView postOrderToDelivery(@PathVariable("id") Optional<String> id_order, @ModelAttribute("courier") String courierID)
    {
        System.out.println(id_order);
        System.out.println(courierID);

        if(id_order.isPresent())
        {
            deliveryDao.save(new Delivery(Integer.parseInt(id_order.get())
                    , Integer.parseInt(courierID)
                    , LocalDateTime.now(), null));
        }

        return getOrder(id_order);
    }


    /**
     *
     * @return объект {@link ModelAndView} - страница с формой создания заказа
     */
    @GetMapping(value = "/createOrder")
    public ModelAndView createOrderForm(@RequestParam(required = false) Customer customerPhone)
    {
        ModelAndView model = new ModelAndView();
        Map<Customer, List<Address>> customerAddress = new HashMap<>();

        List<Customer> customerList = customerDAO.getAll();

        customerList.stream()
                .forEach(x->customerAddress.put(x, customerDAO.getAll(x)));

        Optional<Customer> optionalCustomer = customerAddress
                .keySet()
                .stream()
                .filter(x->x.equals(customerPhone))
                .findFirst();

        model.addObject("customers", customerAddress);
        model.addObject("order", new Order());

        if(optionalCustomer.isPresent())
        {
            model.addObject("addresses", customerAddress.get(optionalCustomer.get()));
        }

        model.setViewName("createOrder");
        return model;
    }


    /**
     *
     * @param order - объект {@link Order} с данными формы для ввода данных нового заказа
     * @return объект {@link ModelAndView}
     */
    @PostMapping(value={"/createOrder", "/createOrder?customerPhone={customerPhone}"})
    public ModelAndView createOrder(@ModelAttribute("order")Order order
            , @RequestParam(required = false) Customer customerPhone
            , @ModelAttribute("addresses") String addresses)
    {
        ModelAndView model = new ModelAndView();

        order.setAddress((Address) customerDAO
                .get(Long.parseLong(addresses)).get());

        try
        {
            model.addObject("id", orderService.save(order).getId());
            model.setViewName("redirect:{id}");

        } catch (DataAccessException ex)
        {
            model.addObject("error", "Не удалось добавить заказ.");
            model.setViewName("createOrder");

        } catch (InvalidOrderStartDate ex)
        {
            String errMsg = messages.getMessage("err.message.invalidOrderStartDate"
                    , null
                    , Locale.getDefault());

            model = createOrderForm(customerPhone);
            model.addObject("order", order);
            model.addObject("addresses", order.getAddress());
            model.addObject("invalidOrderStartDate", errMsg);
            model.setViewName("createOrder");
        }
        return model;
    }

    /**
     *
     * @param order - объект {@link Order} с данными заказа
     * @return объект {@link ModelAndView}
     */
    @PostMapping(value="/editStatus")
    public ModelAndView updateOrderStatus(@ModelAttribute("order")Order order)
    {
        orderService.update(order);
        System.out.println(order);
        historyDao.save(order);
        ModelAndView model = new ModelAndView();
        model.addObject("id", order.getId());
        model.setViewName("redirect:{id}");
        return model;
    }


}
