package delivery.controller;

import delivery.dao.DeliveryDao;
import delivery.dao.OrderDAO;
import delivery.dao.StatusHistoryDAO;
import delivery.dao.UserDAO;
import delivery.model.Delivery;
import delivery.model.Order;
import delivery.model.Status;
import delivery.model.StatusHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Optional;


@RestController
@RequestMapping("/courier/deliverylist")

public class DeliveryServiceController
{
    @Autowired
    OrderDAO orderDAO;
    @Autowired
    DeliveryDao deliveryDao;
    @Autowired
    StatusHistoryDAO historyDao;
    @Autowired
    UserDAO userDAO;



    /**
     * При переходе по url courier/deliverylist/createdelivery вызывается createDelivery.html
     * @return объект {@link ModelAndView}
     */

    @GetMapping(value = "/createdelivery")
    public ModelAndView createDelivery()
    {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userLogin = authentication.getName();
        ModelAndView model = new ModelAndView();
        model.addObject("orders", orderDAO.getAll());
        model.setViewName("createDelivery");
        return model;
    }

    /**
     * При переходе по url courier/deliverylist/createnewdelivery вызывается orderDetailsForDelivery.html
     * @return объект {@link ModelAndView}
     */
    @GetMapping (value="/createnewdelivery")
    public @ResponseBody
    ModelAndView getCreateNewDelivery(@RequestParam(value = "idO")  Optional<String> idO, @RequestParam(value = "idU")  Optional<String> idU) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userLogin = authentication.getName();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.US);
        Order order = new Order();
        try {
            ModelAndView modelView = new ModelAndView();
            long idOr = Long.parseLong(idO.get());
            long idUs = userDAO.get(userLogin).get().getId();

            if (idOr == 0){
                modelView.setStatus(HttpStatus.NOT_FOUND);
                return modelView;
            }

            Delivery delivery = new Delivery((int)idOr,(int)idUs, LocalDateTime.parse("2000-01-01 00:00:00", formatter), LocalDateTime.parse("2000-01-01 00:00:00", formatter));
            delivery.setDateDeliveredStart(LocalDateTime.parse(LocalDateTime.now().toString()));
            Optional<Order> orderFromDB = orderDAO.get(delivery.getIdOrder());
            if (orderFromDB.isPresent()) {
                order = orderFromDB.get();
            }

            if (order.getStatus().equals(Status.TO_DELIVERY)||order.getStatus().equals(Status.DELIVERED)) {
                modelView.setStatus(HttpStatus.OK);
                modelView.setViewName("errOrderDeliv");
                modelView.addObject("mess", "Заказ уже находится в доставке или уже доставлен");
                return modelView;
            } else {

                deliveryDao.save(delivery);
            }
            order.setStatus(Status.TO_DELIVERY);
            orderDAO.update(order);
            historyDao.save(order);
            List<StatusHistory> history = historyDao.getAllByOrder(delivery.getIdOrder());

            if(orderFromDB.isPresent()){
                modelView.setStatus(HttpStatus.OK);
                modelView.setViewName("orderDetailsForDelivery");
                modelView.addObject("order", orderFromDB.get());;
                modelView.addObject("deliveries", delivery);
                modelView.addObject("statusHistory", history);
                modelView.addObject("statusList", Status.getCodeDescription());
            }
            return modelView;
        } catch (NumberFormatException ex){
            throw new NumberFormatException();
        }
    }

    /**
     * При переходе по url courier/deliverylist/info вызывается orderDetailsForDelivery.html
     * @return объект {@link ModelAndView}
     */
    @GetMapping(value="/info")
    public @ResponseBody
    ModelAndView getDeliver(@RequestParam(value = "idO")  Optional<String> idO, @RequestParam(value = "idU")  Optional<String> idU) {


        try {
            ModelAndView modelView = new ModelAndView();
            long idOr = Long.parseLong(idO.get());
            long idUs = Long.parseLong(idU.get());

            if (idOr == 0){
                modelView.setStatus(HttpStatus.NOT_FOUND);
                return modelView;
            }
            Optional<Delivery> deliveryOptional = deliveryDao.get(idOr,idUs);
            Optional<Order> order = orderDAO.get(deliveryOptional.get().getIdOrder());
            List<StatusHistory> history = historyDao.getAllByOrder(deliveryOptional.get().getIdOrder());

            if(order.isPresent()){
                modelView.setStatus(HttpStatus.OK);
                modelView.setViewName("orderDetailsForDelivery");
                modelView.addObject("order", order.get());
                modelView.addObject("deliveries", deliveryOptional.get());
                modelView.addObject("statusHistory", history);
                modelView.addObject("statusList", Status.getCodeDescription());
            }
            return modelView;
        } catch (NumberFormatException ex) {
            throw new NumberFormatException();
        }
    }

    /**
     * При переходе по url courier/deliverylist/all  вызывается deliverylist.html
     * @return объект {@link ModelAndView}
     */
    @GetMapping(value = "/all")
    public ModelAndView getDeliveryList(){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userLogin = authentication.getName();
        long idUs =  userDAO.get(userLogin).get().getId();

        ModelAndView model = new ModelAndView();
        model.addObject("deliveries", deliveryDao.getByUserId(idUs));

        model.setViewName("deliverylist");
        return model;
    }

    /**
     * При переходе по url courier/deliverylist/newDelivery если заказ уже создан вызывается errOrderDeliv.html иначе courier/deliverylist/orderDetailsForDelivery
     * @return объект {@link ModelAndView}
     */
    @GetMapping(value="/newDelivery")
    public @ResponseBody
    ModelAndView getNewDeliver(@RequestParam(value = "idO")  Optional<String> idO) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userLogin = authentication.getName();

        long idOr = Long.parseLong(idO.get());
        long idUs =  userDAO.get(userLogin).get().getId();
        try {
            ModelAndView modelView = new ModelAndView();
            if (idOr == 0){
                modelView.setStatus(HttpStatus.NOT_FOUND);
                return modelView;
            }

            Delivery delivery = new Delivery();
            Optional<Delivery> deliveryOptional = deliveryDao.get(idOr,idUs);
            Optional<Order> orderFromDB = orderDAO.get(idOr);
            Order order = orderFromDB.get();
            if (order.getStatus().equals(Status.DELIVERED)){
                modelView.setStatus(HttpStatus.OK);
                modelView.setViewName("errOrderDeliv");
                modelView.addObject("mess", "Заказ уже доставлен или находится в доставке");
                return modelView;
            } else {
                delivery = deliveryDao.update(deliveryOptional.get()).get();
            }

            order.setStatus(Status.DELIVERED);
            orderDAO.update(order);
            historyDao.save(order);
            List<StatusHistory> history = historyDao.getAllByOrder(deliveryOptional.get().getIdOrder());
            modelView.setStatus(HttpStatus.OK);
            modelView.setViewName("orderDetailsForDelivery");
            modelView.addObject("order", orderFromDB.get());
            modelView.addObject("deliveries", delivery);
            modelView.addObject("statusHistory", history);
            modelView.addObject("statusList", Status.getCodeDescription());
            return modelView;

        } catch (NumberFormatException ex){
            throw new NumberFormatException();
        }
    }



}
