package delivery.controller;

import delivery.dao.CustomerDAO;
import delivery.dao.UserDAO;
import delivery.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    CustomerDAO customerDAO;

    @Autowired
    UserDAO userDAO;



    /**
     * Список всех клиентов
     *
     * @return
     */
    @GetMapping("/all")
    public ModelAndView getAllClient() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("users", userDAO.getAllClient());
        modelAndView.setViewName("allClient");
        return modelAndView;
    }

    /**
     * Просмотр данных клиента
     *
     * @param
     * @return
     */
    @GetMapping(value = "/customer")
    public  ModelAndView getClient() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("users", userDAO.getAllClient().get(1));
        modelAndView.setViewName("customer");
        return modelAndView;
    }

    @PostMapping(value = "/customer")
    public  ModelAndView postClient(@ModelAttribute  User user) {
        if(userDAO.getAllClient().size()>= user.getId()) {
            User users = userDAO.getAllClient().get((int) user.getId() - 1);
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.addObject("users", users);
            modelAndView.setViewName("customer");
            return modelAndView;
        } else {
            user.setLogin("Слишком большой id");
            user.setEmail(" " + userDAO.getAllClient().size());
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.addObject("users", user);

            modelAndView.setViewName("customer");
            return modelAndView;
        }

    }

    @GetMapping("/address")
    public ModelAndView getAddress() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("saveAddress",new Address());
        modelAndView.addObject("message", "Добавить адрес");
        modelAndView.setViewName("address");
        return modelAndView;
    }

    @PostMapping("/address")
    public ModelAndView createAddress(@ModelAttribute Address address) {
        List<Address> list = customerDAO.getAllAddress();
        Address lastAddress = list.get(list.size()-1);
        address.setId_address(lastAddress.getId_address()+1);
        customerDAO.save(address);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("saveAddress",  address);
        modelAndView.addObject("message", "Адрес добавлен");
        modelAndView.setViewName("address");
        return modelAndView;
    }
}