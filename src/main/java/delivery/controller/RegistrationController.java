package delivery.controller;

import delivery.exceptions.UserAlreadyExistsException;
import delivery.model.Customer;
import delivery.model.User;
import delivery.model.UserBuilder;
import delivery.model.UserCreator;
import delivery.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Locale;

@RestController
@RequestMapping("/")
public class RegistrationController
{
    @Autowired
    MessageSource messages;


    IUserService<User, Customer> service;

    @Autowired
    public RegistrationController(IUserService<User, Customer> service)
    {
        this.service = service;
    }

    /**
     * При переходе по url /registration вызывается registration.html
     * @return объект {@link ModelAndView}
     */
    @GetMapping(value = "/registration")
    public ModelAndView getRegistration()
    {
        ModelAndView modelAndView = new ModelAndView();

        User customer = new User();

        modelAndView.addObject("customer", customer);

        modelAndView.setViewName("registration");

        return modelAndView;
    }

    /**
     * Форма регистрации курьера
     * @return {@link ModelAndView} с представлением registration
     */
    @GetMapping(value = "orders/courierAll/registration")
    public ModelAndView getCourierRegistration()
    {
        ModelAndView model = new ModelAndView();

        User courier = new User();

        model.addObject("courier", courier);

        model.setViewName("courierRegistration");

        return model;
    }

    /**
     * Форма регистрации курьера
     * @return {@link ModelAndView} с представлением registration
     */
    @PostMapping(value = "orders/courierAll/registration")
    public ModelAndView postCourierRegistration(@ModelAttribute User user) {
        ModelAndView model = new ModelAndView();

        UserBuilder builder = new UserBuilder();

        UserCreator.createCourier(builder
                , user.getEmail()
                , user.getLogin()
                , user.getPassword());

        try
        {
            this.service.registerNewAccount(builder.build());

        } catch (UserAlreadyExistsException ex)
        {
            String errMsg = messages.getMessage("err.message.userAlreadyExists"
                    , null
                    , Locale.getDefault());

            model.addObject("userAlreadyExists", errMsg);
            model.addObject("courier", user);
            model.setViewName("courierRegistration");

            return model;

        }
        model.setViewName("redirect:/orders/courierAll");

        return model;
    }

    /**
     * Регистрация клиента, как пользователя приложения.
     * Создаётся объект {@link User}, с ролью CUSTOMER
     * @return объект {@link ModelAndView}
     */
    @PostMapping(value = "/registration")
    public ModelAndView postRegistration(@ModelAttribute User user, @ModelAttribute Customer customer)
    {
        ModelAndView model = new ModelAndView();

        UserBuilder builder = new UserBuilder();

        UserCreator.createCustomer(builder
                , user.getEmail()
                , user.getLogin()
                , user.getPassword());
        try
        {
            this.service.registerNewClientAccount(builder.build(), customer);

        }
        catch (UserAlreadyExistsException ex)
        {
            String errMsg = messages.getMessage("err.message.userAlreadyExists"
                    , null
                    , Locale.getDefault());

            model.addObject("userAlreadyExists", errMsg);
            model.addObject("customer", user);
            model.setViewName("registration");

            return model;
        }
        catch (IllegalArgumentException ex)
        {
            String errMsg = messages.getMessage("err.message.incorrectPhone"
                    , null
                    , Locale.getDefault());
            model.addObject("incorrectPhone", errMsg);
        }

        model.addObject("login", user.getLogin());

        model.setViewName("login");

        return model;
    }
}
