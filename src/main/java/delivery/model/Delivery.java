package delivery.model;


import java.time.LocalDateTime;

/**
 * Класс отвечающий за реализацию процедуры доставки
 *  {@link Delivery#idOrder} поле класса, которе хранит уникальный идентификатор заказа
 *  {@link Delivery#idUser} поле класса, которе хранит уникальный идентификатор пользователя
 *  {@link Delivery#dateDeliveredStart} поле класса, которе хранит дату формирования запроса на доставку
 *  {@link Delivery#dateDeliveredEnd} поле класса, которе хранит дату доставки пользвоателю
 */

public class Delivery {


    private Integer idOrder;
    private Integer idUser;
    private LocalDateTime dateDeliveredStart;
    private LocalDateTime dateDeliveredEnd;

    public Delivery(){}

    public Delivery( Integer idOrder, Integer idUser, LocalDateTime dateDeliveredStart, LocalDateTime dateDeliveredEnd) {
        this.idOrder = idOrder;
        this.idUser = idUser;
        this.dateDeliveredStart = dateDeliveredStart;
        this.dateDeliveredEnd = dateDeliveredEnd;
    }




    public Integer getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Integer idOrder) {
        this.idOrder = idOrder;
    }

    public Integer getIdUser() { return idUser; }

    public void setIdUser(Integer idUser) { this.idUser = idUser; }

    public LocalDateTime getDateDeliveredStart() {
        return dateDeliveredStart;
    }

    public void setDateDeliveredStart(LocalDateTime dateDeliveredStart) {
        this.dateDeliveredStart = dateDeliveredStart;
    }

    public LocalDateTime getDateDeliveredEnd() {
        return dateDeliveredEnd;
    }

    public void setDateDeliveredEnd(LocalDateTime dateDeliveredEnd) {
        this.dateDeliveredEnd = dateDeliveredEnd;
    }


    /**
     * Информация об объекте
     * @return информация об объекте, тип данных {@link String}
     */
    @Override
    public String toString()
    {
        return "Заказ № "
                + getIdOrder()
                + " идентификатор пользователя "
                + getIdUser()
                + " дата оформления доставки "
                + getDateDeliveredStart()
                + " дата доставки "
                + getDateDeliveredEnd();
    }

    /**
     * Метод сравнения вух объектов
     *
     * @param o -сравниваемый объект класса {@link Object}
     * @return результат сравнения {@link Boolean}
     */
    @Override
    public boolean equals(Object o)
    {
        if(this == o)
        {
            return true;
        }
        if (o == null || o.getClass() != this.getClass())
        {
            return false;
        }

        Delivery delivery = (Delivery) o;

        return this.idUser.equals(delivery.idUser)
                && this.idOrder.equals(delivery.idOrder)
                && this.dateDeliveredStart.equals(delivery.dateDeliveredStart);
    }

}
