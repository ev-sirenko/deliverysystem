package delivery.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum Role
{
    ADMINISTRATOR(1L),
    COURIER(2L),
    CUSTOMER(3L);

    private final long code;

    Role(long code)
    {
        this.code = code;
    }

    public long getCode()
    {
        return code;
    }

    /**
     * Возврат объекта Role по коду
     * @param code - код статуса
     * @return - объект Status
     */
    public static Role getRoleByCode(long code)
    {
        return Arrays.stream(values()).filter(x-> x.getCode() == code).findFirst().get();
    }

}
