package delivery.model;

public class UserCreator
{
    public static void createAdministrator(UserBuilder builder, String eMail, String login, String password)
    {
        builder.setEMail(eMail);
        builder.setLogin(login);
        builder.setPassword(password);
        builder.setRole("administrator");
    }

    public static void createCourier(UserBuilder builder, String eMail, String login, String password)
    {
        builder.setEMail(eMail);
        builder.setLogin(login);
        builder.setPassword(password);
        builder.setRole("courier");
    }

    public static void createCustomer(UserBuilder builder, String eMail, String login, String password)
    {
        builder.setEMail(eMail);
        builder.setLogin(login);
        builder.setPassword(password);
        builder.setRole("customer");
    }

}
