package delivery.model;

import java.util.List;

public class Customer
{
    private String phone;
    private List<Address> addressList;

    public Customer()
    {
    }

    public Customer(String phone)
    {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString()
    {
        return phone;
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o)
        {
            return true;
        }
        if (o == null || o.getClass() != this.getClass())
        {
            return false;
        }
        Customer p = (Customer) o;

        return this.phone.equals(p.phone);
    }

}
