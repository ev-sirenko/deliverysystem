package delivery.model;

public class CustomerAddress {

    private String phone;
    private long id_address;

    public CustomerAddress(String phone, long id_address) {
        this.phone = phone;
        this.id_address = id_address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public long getId_address() {
        return id_address;
    }

    public void setId_address(long id_address) {
        this.id_address = id_address;
    }

    @Override
    public String toString() {
        return "CustomerAddress{" +
                "phone='" + phone + '\'' +
                ", id_address=" + id_address +
                '}';
    }
}
