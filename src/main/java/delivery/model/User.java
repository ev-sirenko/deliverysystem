package delivery.model;

public class User
{
    private long id;
    private String login;
    private String password;
    private String email;
    private Role role;

    public User()
    {
    }

    public User(long id, String login, String password, String email, Role role)
    {
        this.id = id;
        this.login = login;
        this.password = password;
        this.email = email;
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString()
    {
        return  getId()
                + " "
                + getLogin()
                + " "
                + getEmail()
                + " "
                + getRole();
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o)
        {
            return true;
        }
        if (o == null || o.getClass() != this.getClass())
        {
            return false;
        }

        User user = (User) o;

        return this.id == user.id
                && this.login.equals(user.login)
                && this.password.equals(user.password);
    }
}
