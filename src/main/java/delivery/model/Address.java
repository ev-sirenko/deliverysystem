package delivery.model;

public class Address
{
    private long id_address;
    private String street;
    private String building;
    private String room;
    private int building_floor;

    public Address(long id_address, String street, String building, String room, int building_floor) {
        this.id_address = id_address;
        this.street = street;
        this.building = building;
        this.room = room;
        this.building_floor = building_floor;
    }

    public Address( String street, String building, String room, int building_floor) {
        this.id_address=1;
        this.street = street;
        this.building = building;
        this.room = room;
        this.building_floor = building_floor;
    }
    public Address() {

    }


    public long getId_address() {
        return id_address;
    }

    public void setId_address(long id_address) {
        this.id_address = id_address;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public int getBuilding_floor() {
        return building_floor;
    }

    public void setBuilding_floor(int building_floor) {
        this.building_floor = building_floor;
    }

    @Override
    public String toString() {
        return  "Улица:'" + street + '\'' +
                ", Строение:'" + building + '\'' +
                ", Квартира:'" + room + '\'' +
                ", этаж:" + building_floor;
    }
}
