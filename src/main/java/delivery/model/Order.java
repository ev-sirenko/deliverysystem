package delivery.model;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Component
public class Order
{
    private long id;
    private Customer customer;
    private Status status;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime created;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastModified;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;
    private String deliveryDesiredTime;
    private Address address;
    private List<Item> items;

    public Order()
    {

    }
    public void setId(long id)
    {
        this.id = id;
    }

    public void setCustomer(Customer customer)
    {
        this.customer = customer;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }

    public void setCreated(LocalDateTime created)
    {
        this.created = created;
    }

    public void setLastModified(LocalDateTime lastModified)
    {
        this.lastModified = lastModified;
    }

    public long getId()
    {
        return id;
    }

    public Status getStatus()
    {
        return status;
    }

    public LocalDateTime getLastModified()
    {
        return lastModified;
    }

    public LocalDateTime getCreated()
    {
        return created;
    }

    public Customer getCustomer()
    {
        return customer;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getDeliveryDesiredTime() {
        return deliveryDesiredTime;
    }

    public void setDeliveryDesiredTime(String deliveryDesiredTime) {
        this.deliveryDesiredTime = deliveryDesiredTime;
    }

    public Address getAddress()
    {
        return address;
    }

    public void setAddress(Address address)
    {
        this.address = address;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString()
    {
        return "Заказ № "
                + this.getId()
                + " "
                + this.getStartDate()
                + " "
                + this.deliveryDesiredTime
                +"."
                + (this.status == null ? " " :getStatus().getDescription());
    }

    @Override
    public boolean equals(Object o)
    {
        if(this == o)
        {
            return true;
        }
        if (o == null || o.getClass() != this.getClass())
        {
            return false;
        }

        Order order = (Order) o;

        return this.id == order.id
            && this.customer.equals(order.customer)
            && this.created.equals(order.created);
    }
}
