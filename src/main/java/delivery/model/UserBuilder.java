package delivery.model;

import delivery.exceptions.RoleNotFoundException;

import java.util.Arrays;
import java.util.Locale;

public class UserBuilder
{

    private long id;
    private String login;
    private String password;
    private String email;
    private Role role;


    public UserBuilder setLogin(String login)
    {
        this.login = login;
        return this;
    }

    public UserBuilder setPassword(String password)
    {
        this.password = password;
        return this;
    }

    public UserBuilder setEMail(String email)
    {
        this.email = email;
        return this;
    }

    public UserBuilder setRole(String role)
    {
        try
        {
            this.role = Arrays.stream(Role.values())
                    .filter(x -> x.name().equals(role.toUpperCase(Locale.ROOT)))
                    .findFirst().orElseThrow(IllegalArgumentException::new);

        } catch (IllegalArgumentException ex)
        {
            System.out.println(new RoleNotFoundException(ex).traceToSring());
        }
        return this;
    }

    /**
     *
     * @return true если все поля или гостевая учётная запись
     */
    private boolean checkData()
    {
        if (this.login == null
                || this.password == null
                || this.email == null
                || this.role == null)
        {
            return false;
        } else return true;
    }

    /**
     *
     * @return объект класса User
     */
    public User build()
    {
        if (checkData())
        {
            return new User(id, login, password, email, role);
        } else
        {
            return null;
        }
    }

}


