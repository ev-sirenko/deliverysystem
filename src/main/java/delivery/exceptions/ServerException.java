package delivery.exceptions;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;


public class ServerException extends Exception
{
    private Exception outException;

    public ServerException(Exception ex) {
        super(ex);
        this.outException = ex;
    }

    public String traceToSring() {
        OutputStream out = new ByteArrayOutputStream();
        this.outException.printStackTrace(new PrintStream(out));
        return out.toString();
    }

    public static ServerException getServerException(Exception exception) {
        ServerException exceptionToThrow = new ServerException(exception);
        System.out.println(exceptionToThrow.traceToSring());
        return exceptionToThrow;
    }

}
