package delivery.exceptions;

public class InvalidOrderStartDate extends RuntimeException
{
    public InvalidOrderStartDate()
    {
    }

    public InvalidOrderStartDate(String message)
    {
        super(message);
    }

    public InvalidOrderStartDate(String message, Throwable cause)
    {
        super(message, cause);
    }

    public InvalidOrderStartDate(Throwable cause)
    {
        super(cause);
    }
}
