package delivery.exceptions;

public class RoleNotFoundException extends ServerException
{

    public RoleNotFoundException(Exception ex)
    {
        super(ex);
    }
}