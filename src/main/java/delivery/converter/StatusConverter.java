package delivery.converter;

import delivery.model.Status;
import org.springframework.core.convert.converter.Converter;

import java.util.Locale;

public class StatusConverter implements Converter<String, Status>
{
    @Override
    public Status convert(String source)
    {
        return Status.valueOf(source.toUpperCase(Locale.ROOT));
    }
}
