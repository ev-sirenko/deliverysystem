package delivery.converter;

import delivery.dao.CustomerDAO;
import delivery.model.Address;
import org.springframework.core.convert.converter.Converter;

import java.util.Optional;

public class AddressConverter implements Converter<String, Address>
{
    final CustomerDAO customerDAO;

    public AddressConverter(CustomerDAO customerDAO)
    {
        this.customerDAO = customerDAO;
    }

    @Override
    public Address convert(String source)
    {
       long addressID = Long.parseLong(source);

        Optional<Address> addressOptional = customerDAO.get(addressID);

        if (addressOptional.isPresent())
        {
            return addressOptional.get();
        }

        return new Address();
    }
}
