package delivery.converter;

import org.springframework.core.convert.converter.Converter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateConverter implements Converter<String, LocalDate>
{

    @Override
    public LocalDate convert(String source)
    {

        return LocalDate.parse(source, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

}
