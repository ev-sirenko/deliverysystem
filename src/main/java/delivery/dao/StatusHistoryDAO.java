package delivery.dao;

import delivery.mapper.StatusHistoryMapper;
import delivery.model.Order;
import delivery.model.StatusHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

public class StatusHistoryDAO
{
    private static final String GET_BY_ORDER_ID = "SELECT h.id_status_history\n" +
            ", h.id_order\n" +
            ", o.phone\n" +
            ", o.status\n" +
            ", o.created_time\n" +
            ", o.last_modified_time\n" +
            ", o.start_date\n" +
            ", o.end_date\n" +
            ", o.id_address\n" +
            ", o.what_time_to_deliver\n" +
            ", h.status as history_status\n" +
            ", h.date_set\n" +
            ", a.street, a.building, a.room, a.building_floor \n" +
            "FROM STATUS_HISTORY h\n" +
            "JOIN ORDERS o ON h.id_order = o.id_order\n" +
            "JOIN ADDRESS a ON a.id_address = o.id_address \n" +
            "WHERE h.id_order = ?";
    private static final String SAVE = "INSERT INTO STATUS_HISTORY\n" +
                                        "(id_order, status, date_set)\n" +
                                            "VALUES(?, ?, datetime('now', 'localtime'));";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public StatusHistoryDAO(JdbcTemplate template)
    {
        this.jdbcTemplate = template;
    }

    public List<StatusHistory> getAllByOrder(long id)
    {
            long idRecord = id;

            List<StatusHistory> statusHistory = jdbcTemplate.query
                    (
                            GET_BY_ORDER_ID
                            , ps -> ps.setLong(1, id)
                            , new StatusHistoryMapper()
                    );

            return statusHistory;
    }


    public Optional<Long> save(Order order)
    {
       Long id = jdbcTemplate.execute(connection ->
               {
                   PreparedStatement ps =
                           connection.prepareStatement(SAVE, Statement.RETURN_GENERATED_KEYS);
                   ps.setLong(1, order.getId());
                   ps.setLong(2, order.getStatus().getCode());
                   return ps;
               }, (PreparedStatementCallback<Long>) ps ->
               {
                   ps.executeUpdate();

                   ResultSet result = ps.getGeneratedKeys();

                   long key = 0;

                   while (result.next()) {
                       key = result.getLong(1);
                   }

                   return key;

                }
       );
        return Optional.ofNullable(id);
    }

}
