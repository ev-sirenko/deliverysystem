package delivery.dao;

import delivery.mapper.AddressMapper;
import delivery.mapper.CustomerAddressRowMapper;
import delivery.mapper.CustomerRowMapper;
import delivery.model.Address;
import delivery.model.Customer;
import delivery.model.CustomerAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CustomerDAO implements Dao {

    private static final String GET = "SELECT * FROM CUSTOMER WHERE PHONE = ?";
    private static final String GET_ALL = "SELECT * FROM CUSTOMER";

    private static final String GET_ADDRESS_PHONE = "Select  * From  ADDRESS " +
            "Inner Join CUSTOMER_ADDRESS " +
            "On CUSTOMER_ADDRESS.id_address = ADDRESS.id_address " +
            "Inner Join  CUSTOMER On CUSTOMER_ADDRESS.phone = CUSTOMER.phone " +
            " where CUSTOMER.phone = ?";

    private static final String GET_ALL_ADDRESS = "select * from address";

    private static final String SAVE =
            "INSERT INTO ADDRESS(id_address, street, building, room, building_floor)\n" +
                    "VALUES (?,?,?,?,?);";

    private static final String SAVE_CUSTOMER =
            "INSERT INTO CUSTOMER(phone)\n" +
                    "VALUES (?);";

    private static final String UPDATE_WITH_PHONE = "UPDATE ADDRESS " +
            "set  street =? , building =?,room =?, building_floor =? " +
            "WHERE id_address = ? ";

    private static final String UPDATE = "UPDATE ADDRESS " +
            "set street =? , building =?,room =?, building_floor =?" +
            "WHERE id_address = ? ";

    private static final String GET_CUSTOMER_ADDRESS =
            "SELECT * from CUSTOMER_ADDRESS ";

    private static final String GET_ADDRESS_ID = "SELECT * FROM ADDRESS WHERE id_address =?";
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public CustomerDAO(JdbcTemplate template) {
        this.jdbcTemplate = template;
    }

    /**
     * Получает телефонный номер
     *
     * @param phone
     * @return
     */
    public Optional<Customer> get(String phone) {
        List<Customer> customers = jdbcTemplate.query
                (
                        GET
                        , ps -> ps.setString(1, phone)
                        , new CustomerRowMapper()
                );

        Optional<Customer> person = customers.stream().findFirst();

        return person;
    }

    /**
     * Получить адрес по id
     *
     * @param id - уникальный идентификатор записи
     * @return
     */
    @Override
    public Optional get(long id) {
        List<Address> addressList = jdbcTemplate.query(GET_ADDRESS_ID, ps -> ps.setLong(1, id), new AddressMapper());
        Optional<Address> address = addressList.stream().findFirst();
        return address;
    }

    /**
     * Получить список всех адресов клиента, по телефону
     *
     * @param parameter
     * @return список
     */

    public List<Address> getAll(Object parameter) throws DataAccessException {
        Customer customer = (Customer) parameter;
        List<Address> addressList = new ArrayList<>();
        addressList = jdbcTemplate.query(GET_ADDRESS_PHONE,
                ps -> ps.setString(1, customer.getPhone()),
                new AddressMapper());
        return addressList;
    }


    /**
     * @return список телефонов
     */
    public List<Customer> getAll() {
        List<Customer> customers;
        customers = jdbcTemplate.query(GET_ALL, new CustomerRowMapper());
        List<Customer> person = customers.stream().collect(Collectors.toList());

        return person;
    }

    /**
     * @return Список всех адресов
     */
    public List<Address> getAllAddress() {

        return jdbcTemplate.query(GET_ALL_ADDRESS, new AddressMapper());
    }


    /**
     * Сохранить адресс
     *
     * @param o
     * @return id нового адреса
     */
    @Override
    public Optional<Long> save(Object o) {
        Address address = (Address) o;
        long id = jdbcTemplate.update(SAVE, address.getId_address(), address.getStreet(), address.getBuilding(),
                address.getRoom(), address.getBuilding_floor());
        return Optional.ofNullable(address.getId_address());
    }

    /**
     * Сохранить заказчика
     *
     * @param customer
     * @return id нового адреса
     */
    public Optional<Customer> saveCustomer(Customer customer) throws IllegalArgumentException
    {
        int affectedRows = jdbcTemplate.update(SAVE_CUSTOMER, customer.getPhone());

        if(affectedRows == 1)
        {
            return this.get(customer.getPhone());
        }
        else
        {
            throw  new IllegalArgumentException("Неверный параметр: customer " + customer);
        }
    }

    /**
     * Изменить адрес по id
     *
     * @param o
     * @return новый адрес
     * @throws SQLException
     */
    @Override
    public Optional update(Object o) {
        Address address = (Address) o;
        long id = jdbcTemplate.update(UPDATE, address.getStreet(), address.getBuilding(),
                address.getRoom(), address.getBuilding_floor(), address.getId_address());
        return Optional.ofNullable(address);
    }


    /**
     * Изменения адреса по телефону
     *
     * @param customer
     * @param address
     * @return телефон
     * @throws SQLException
     */
    public Optional<Customer> updateAddressWithPhone(Customer customer, Address address)  {
        List<CustomerAddress> customerAddresses =
                jdbcTemplate.query(GET_CUSTOMER_ADDRESS, new CustomerAddressRowMapper());

        long id_address = customerAddresses.stream()
                .filter(customerAddress -> customerAddress.getPhone().equals(customer.getPhone()))
                .collect(Collectors.toList())
                .get(0).getId_address();

        jdbcTemplate.update(UPDATE_WITH_PHONE, address.getStreet(), address.getBuilding(),
                address.getRoom(), address.getBuilding_floor(), id_address);

        return this.get(customer.getPhone());
    }


}

