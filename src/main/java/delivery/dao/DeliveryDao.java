package delivery.dao;

import delivery.mapper.DeliveryRowMapper;
import delivery.model.Delivery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

/**
 * Класс, в котором формируются запросы к базе данны для работы с классом {@link Delivery}
 * Иплементирует интерфейс {@link Dao}
 */

public class DeliveryDao implements Dao<Delivery>{

    private static final String GET = "SELECT * FROM delivery_service WHERE id_order=? AND id_user=?";
    private static final String GET_BY_USER = "SELECT * FROM delivery_service WHERE id_user=?";
    private static final String GET_ALL = "SELECT * FROM delivery_service";
    private static final String SAVE = "INSERT INTO delivery_service\n" +
            "(id_order, id_user, start_date, end_date)\n" +
            "VALUES(?, ?, ?, ?);";
    private static final String UPDATE = "UPDATE delivery_service\n" +
            "SET end_date=?" +
            " WHERE id_order=? AND id_user=?;";


    private JdbcTemplate jdbcTemplate;

    @Autowired
    public DeliveryDao(JdbcTemplate template)
    {
        this.jdbcTemplate = template;
    }

    /**
     * Формирование о обработка запроса {@link DeliveryDao#GET} по уникальному идентификатору {@link Delivery#getIdOrder()}  и {@link Delivery#getIdUser()}
     *
     * @param id_order,id_user - уникальные идентификатор записи
     * @return объект класса {@link Delivery}
     */
    public Optional<Delivery> get(long id_order,long id_user) {

        List<Delivery> deliveries = jdbcTemplate.query
                (
                        GET
                        , ps -> {ps.setLong(1, id_order);ps.setLong(2, id_user);}
                        , new DeliveryRowMapper()
                );
        return deliveries.stream().filter(x-> ((x.getIdOrder() == id_order) && (x.getIdUser()==id_user))).findFirst();
    }

    /**
     * Формирование о обработка запроса {@link DeliveryDao#GET_BY_USER} по уникальному идентификатору  {@link Delivery#getIdUser()}
     *
     * @param id_user - уникальные идентификатор записи
     * @return объект класса {@link Delivery}
     */
    public List<Delivery> getByUserId(long id_user) {

        List<Delivery> deliveries = jdbcTemplate.query
                (
                        GET_BY_USER
                        , ps -> {ps.setLong(1, id_user);}
                        , new DeliveryRowMapper()
                );
        return deliveries;


    }

    /**
     * Формирование о обработка SQL запроса {@link DeliveryDao#GET_ALL} по всем полям записи
     * @return объект класса {@link Delivery}
     */
    @Override
    public List<Delivery> getAll() {
        List<Delivery> deliveries = null;
        deliveries = jdbcTemplate.query(GET_ALL, new DeliveryRowMapper());
        return deliveries;
    }


    /**
     *  Формирование о обработка SQL запроса {@link DeliveryDao#SAVE}
     *  запись в базу данных объекта класса {@link Delivery}
     *
     * @param delivery - объект класса {@link Delivery}
     * @return объект класса {@link Optional}
     */
    @Override
    public Optional<Long> save(Delivery delivery) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        Long id = jdbcTemplate.execute(connection ->
                {
                    PreparedStatement ps =
                            connection.prepareStatement(SAVE, Statement.RETURN_GENERATED_KEYS);
                            ps.setInt(1, delivery.getIdOrder());
                            ps.setInt(2, delivery.getIdUser());
                            ps.setString(3, (delivery.getDateDeliveredStart()==null) ? "" :delivery.getDateDeliveredStart().format(formatter));
                            ps.setString(4, (delivery.getDateDeliveredEnd()==null) ? "" : delivery.getDateDeliveredEnd().format(formatter));
                    return ps;
                }, (PreparedStatementCallback<Long>) ps ->
                {
                    ps.executeUpdate();
                    ResultSet result = ps.getGeneratedKeys();
                    long key = 0;
                    while (result.next()) {
                        key = result.getLong(1);
                    }
                    return key;
                }
        );
        return Optional.of(id);
    }


    /**
     *  Формирование о обработка SQL запроса {@link DeliveryDao#UPDATE}
     *  обновление поля  {@link Delivery#setDateDeliveredEnd(LocalDateTime)}
     *  в базе данных после доставки заказа, по уникальному ID доставки
     *  ставится текущая дата
     *
     * @param delivery - объект класса {@link Delivery}
     * @return объект класса {@link Optional}
     */
    @Override
    public Optional<Delivery> update(Delivery delivery) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        Delivery deliveryUpdate = new Delivery(delivery.getIdOrder(), delivery.getIdUser(), null, LocalDateTime.now());
        final int affectedRows = jdbcTemplate.update(UPDATE, deliveryUpdate.getDateDeliveredEnd().format(formatter), deliveryUpdate.getIdOrder(), deliveryUpdate.getIdUser());
        if (affectedRows == 1){
            return this.get(delivery.getIdOrder(),delivery.getIdUser());
        }
        return Optional.empty();
    }


    @Override
    public Optional<Delivery> get(long id) throws DataAccessException {
        return Optional.empty();
    }


}
