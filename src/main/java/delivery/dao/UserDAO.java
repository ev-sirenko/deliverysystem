package delivery.dao;

import delivery.mapper.UserRowMapper;
import delivery.model.Role;
import delivery.model.User;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

public class UserDAO implements Dao<User>
{
    private static final String GET_BY_LOGIN = "SELECT id_user, login, password, email, user_role\n" +
                                                "FROM USER\n" +
                                                "WHERE login = ? ";

    private static final String GET_BY_ID = "SELECT id_user, login, password, email, user_role\n" +
            "FROM USER\n" +
            "WHERE id_user = ? ";

    private static final String GET_ALL = "SELECT id_user, login, password, email, user_role\n" +
            "FROM USER\n";

    private static final String GET_ALL_BY_ROLE = "SELECT id_user, login, password, email, user_role\n" +
            "FROM USER\n" +
            "WHERE user_role = ? ";

   private static final String SAVE = "INSERT INTO USER\n" +
                                    "(login, password, email, user_role)\n" +
                                    "VALUES(?, ?, ?, ?);";

    private static final String GET_ALL_CLIENT = "SELECT id_user, login, password, email, user_role\n" +
            "FROM USER\n" +
            "WHERE user_role = 3 ";

    private final JdbcTemplate jdbcTemplate;


    public UserDAO(JdbcTemplate template)
    {
        this.jdbcTemplate = template;
    }


    @Override
    public Optional<User> get(long id) throws DataAccessException
    {
        Optional<User> user;

        user = jdbcTemplate.query(
                GET_BY_ID
                , ps -> ps.setString(1, Long.toString(id))
                , new UserRowMapper()).stream().findFirst();
        return user;

    }

    public List<User> getAllClient(){
        return jdbcTemplate.query(GET_ALL_CLIENT, new UserRowMapper());
    }

    /**
     * Получить пользователя с лоигином = login
     * @param login - логин пользователя
     * @return - Optional<User>
     * @throws DataAccessException
     */
    public Optional<User> get(String login) throws DataAccessException
    {
        return jdbcTemplate.query(
                GET_BY_LOGIN
                , ps -> ps.setString(1, login)
                , new UserRowMapper()).stream().findFirst();
    }

    @Override
    public List<User> getAll() throws DataAccessException
    {
        return  jdbcTemplate.query(GET_ALL, new UserRowMapper());

    }


    public List<User> getAll(Role role) throws DataAccessException
    {
        return jdbcTemplate.query(
                GET_ALL_BY_ROLE
                , ps -> ps.setString(1, String.valueOf(role.getCode()))
                , new UserRowMapper());
    }

    @Override
    public Optional<Long> save(User user) throws DataAccessException
    {
       Long id = jdbcTemplate.execute(connection ->
            {
                PreparedStatement ps =
                        connection.prepareStatement(SAVE, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, user.getLogin());
                ps.setString(2, user.getPassword());
                ps.setString(3, user.getEmail());
                ps.setLong(4, user.getRole().getCode());
                return ps;
            }, (PreparedStatementCallback<Long>) ps ->
            {
                ps.executeUpdate();

                ResultSet result = ps.getGeneratedKeys();

                long key = 0;

                while (result.next()) {
                    key = result.getLong(1);
                }

                return key;

            }
    );
        return Optional.ofNullable(id);
    }

    @Override
    public Optional<User> update(User user) throws DataAccessException
    {
        return Optional.empty();
    }
}
