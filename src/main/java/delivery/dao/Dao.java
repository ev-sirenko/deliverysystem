package delivery.dao;

import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Optional;

public interface Dao<T>
{

    /**
     * Запрашивает запись по указанному id.
     * @param id - уникальный идентификатор записи
     * @return объект класса {@link Optional <T>}
     * @throws DataAccessException
     */
    Optional<T> get(long id) throws DataAccessException;


    /**
     * Запрашивает все записи
     * @return - список записей типа T
     * @throws DataAccessException
     */
    List<T> getAll() throws DataAccessException;

    /**
     * Сохраняет объект, создавая новую запись
     * @param t - объект. который нужно сохранить
     * @throws DataAccessException
     * @return - id вставленной записи
     */
    Optional<Long> save(T t) throws DataAccessException;

    /**
     * Изменяет запись с @param id
     * @param t - объект, содержащий измененные поля
     * @throws DataAccessException
     */
    Optional<T> update(T t) throws DataAccessException;

}