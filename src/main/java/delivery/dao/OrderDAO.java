package delivery.dao;


import delivery.converter.StatusConverter;
import delivery.log.LogWriter;
import delivery.mapper.OrderRowMapper;
import delivery.model.Order;
import delivery.model.Status;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Обработка запросов к БД по таблице Orders
 */
public class OrderDAO implements IOrderDAO<Order>
{
    private static final String GET = "SELECT o.id_order\n" +
                                    ", o.phone\n" +
                                    ", o.status\n" +
                                    ", o.created_time\n" +
                                    ", o.last_modified_time\n" +
                                    ", o.start_date\n" +
                                    ", o.end_date\n" +
                                    ", o.what_time_to_deliver\n" +
                                    ", a.id_address,\n" +
                                    "a.street, a.building, a.room, a.building_floor \n" +
                                    "FROM ORDERS o\n" +
                                    "JOIN ADDRESS a ON a.id_address = o.id_address \n" +
                                    "WHERE ID_ORDER = ?";

    private static final String GET_ALL ="SELECT o.id_order\n" +
                                    ", o.phone\n" +
                                    ", o.status\n" +
                                    ", o.created_time\n" +
                                    ", o.last_modified_time\n" +
                                    ", o.start_date\n" +
                                    ", o.end_date\n" +
                                    ", o.what_time_to_deliver\n" +
                                    ", a.id_address,\n" +
                                    "a.street, a.building, a.room, a.building_floor \n" +
                                    "FROM ORDERS o\n" +
                                    "JOIN ADDRESS a ON a.id_address = o.id_address";

    private static final String SAVE = "INSERT INTO ORDERS\n" +
                                        "(phone, status, created_time, last_modified_time, start_date, end_date, what_time_to_deliver, id_address)\n" +
                                        "VALUES(?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String UPDATE = "UPDATE ORDERS\n" +
                                        "SET status=?, last_modified_time=?\n" +
                                        "WHERE id_order=?;";

    final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    final DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private final JdbcTemplate jdbcTemplate;

    private final LogWriter logWriter;

    public OrderDAO(JdbcTemplate template, LogWriter logWriter)
    {
        this.jdbcTemplate = template;
        this.logWriter = logWriter;
    }

    @Override
    public Optional<Order> get(long id) throws DataAccessException
    {
        final List<Order> orders = jdbcTemplate.query
                (
                    GET
                    , ps -> ps.setLong(1, id)
                    , new OrderRowMapper()
                );

        logWriter.writeInfo(this.getClass().getName() + "; method: get(id); query:\n" + GET + "(" + id  +")");

        return orders.stream().filter(x-> x.getId() == id).findFirst();
    }


    public List<Order> getAll(Map<String, String> parameters) throws DataAccessException
    {
        final StringBuilder query = new StringBuilder(GET_ALL).append("\nWHERE ");

        if (parameters.containsKey("start_date"))
        {
            query.append(" start_date = '").append(parameters.get("start_date")).append("'");
        }

        if (parameters.containsKey("customer"))
        {
            query.append(" AND customer = ").append(parameters.get("customer"));
        }

        if (parameters.containsKey("status"))
        {
            final StatusConverter statusConverter = new StatusConverter();

            Status filterStatus = statusConverter.convert(parameters.get("status"));

            query.append(" AND status = ").append(filterStatus.getCode());
        }

        logWriter.writeInfo(this.getClass().getName() + "; method: getAll(Map); query:\n" + query);

        return jdbcTemplate.query(query.toString(), new OrderRowMapper());
    }


    @Override
    public List<Order> getAll() throws DataAccessException
    {
        return jdbcTemplate.query(GET_ALL, new OrderRowMapper());
    }

    @Override
    public Optional<Long> save(Order order) throws DataAccessException
    {
        final Long id = jdbcTemplate.execute(connection ->
                {
                    PreparedStatement ps =
                            connection.prepareStatement(SAVE, Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, order.getCustomer().getPhone());
                    ps.setLong(2, Status.CREATED.getCode());
                    ps.setString(3, LocalDateTime.now().format(dateTimeFormat));
                    ps.setString(4, LocalDateTime.now().format(dateTimeFormat));
                    ps.setString(5, order.getStartDate().toString());
                    ps.setString(6, order.getEndDate().toString());
                    ps.setString(7, order.getDeliveryDesiredTime());
                    ps.setLong(8, order.getAddress().getId_address());

                    return ps;
                }, (PreparedStatementCallback<Long>) ps ->
                {
                    ps.executeUpdate();

                    ResultSet result = ps.getGeneratedKeys();

                    long key = 0;

                    while (result.next()) {
                        key = result.getLong(1);
                    }

                    return key;

                }
        );
        logWriter.writeInfo(this.getClass().getName() + "; method: get(id); query:\n" + SAVE);
        return Optional.ofNullable(id);
    }


    @Override
    public Optional<Order> update(Order order) throws DataAccessException
    {
        final int affectedRows = jdbcTemplate.update(UPDATE
                , order.getStatus().getCode()
                , LocalDateTime.now().format(dateTimeFormat)
                , order.getId());

        logWriter.writeInfo(this.getClass().getName() + "; method: update(Order); query:\n"
                + UPDATE
                + "("
                + order.getStatus().getCode()
                +", "
                + LocalDate.now()
                + ", "
                + order.getId()
                + ")");

        return get(order.getId());
    }

}
