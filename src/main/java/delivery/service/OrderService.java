package delivery.service;

import delivery.dao.OrderDAO;
import delivery.exceptions.InvalidOrderStartDate;
import delivery.exceptions.OrderNotFoundException;
import delivery.model.Order;
import delivery.model.Status;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

public class OrderService implements IOrderService<Order>
{
    final OrderDAO orderDAO;

    public OrderService(OrderDAO orderDAO)
    {
        this.orderDAO = orderDAO;
    }

    private Predicate<LocalDate> deliveryStartDateNotInPast()
    {
        return x -> x.compareTo(LocalDate.now()) >= 0;
    }


    @Override
    public Order get(long id) throws OrderNotFoundException
    {
        final Optional<Order> orderOptional = orderDAO.get(id);

        if(orderOptional.isPresent())
        {
            return orderOptional.get();
        } else
        {
            throw new OrderNotFoundException("Заказ с номером " + id + " не найден");

        }

    }

    @Override
    public List<Order> getAll(Map<String, String> parameters)
    {
        if(parameters.isEmpty())
        {
            return orderDAO.getAll();
        }

        return orderDAO.getAll(parameters);

    }

    @Override
    public List<Order> getAll()
    {
        return orderDAO.getAll();

    }


    @Override
    @Transactional(rollbackFor = DataAccessException.class)
    public Order save(Order order) throws OrderNotFoundException, InvalidOrderStartDate
    {
        if (order.getEndDate() == null)
        {
            order.setEndDate(order.getStartDate());
        }

        final Optional<Long> orderID = orderDAO.save(order);

        if(!this.deliveryStartDateNotInPast().test(order.getStartDate()))
        {
            throw new InvalidOrderStartDate("Дата доставки не может быть меньше текущей даты");
        }

        if (orderID.isPresent())
        {
            return  this.get(orderID.get());
        } else
        {
            throw new OrderNotFoundException("Возможно в процессе добавления заказа произошла ошибка");
        }
    }

    @Override
    @Transactional(rollbackFor = DataAccessException.class)
    public Order update(Order order) throws OrderNotFoundException
    {
        try
        {
            final Optional<Order> orderOptional = orderDAO.update(order);

            if(orderOptional.isPresent())
            {
                return orderOptional.get();
            }
            else
            {
                throw new OrderNotFoundException("Заказ не найден");
            }
        }
        catch (DataAccessException ex)
        {
            return this.get(order.getId());
        }
    }

    @Override
    public Order delete(Order order)
    {
        order.setStatus(Status.CANCELED);
        return  this.update(order);
    }

}
