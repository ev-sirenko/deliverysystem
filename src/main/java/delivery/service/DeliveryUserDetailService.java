package delivery.service;

import delivery.dao.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Optional;


@Component
public class DeliveryUserDetailService implements UserDetailsService
{
    @Autowired
    UserDAO users;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        Optional<delivery.model.User> user = users.get(username.toLowerCase());

        if(!user.isPresent())
        {
            throw new UsernameNotFoundException("Пользователь не найден");
        }
        delivery.model.User found = user.get();

        return new User(found.getLogin()
                , found.getPassword()
                , Collections.singleton(new SimpleGrantedAuthority(found.getRole().name()))
        );
    }

}
