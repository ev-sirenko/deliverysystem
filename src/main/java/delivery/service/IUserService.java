package delivery.service;

import delivery.exceptions.UserAlreadyExistsException;
import delivery.model.User;

import java.util.List;
import java.util.Optional;

public interface IUserService<T, N>
{
    /**
     * Регистрация нового пользователя
     * @param t -
     * @return - объект типа {@link Optional<T>} - содержащий данные зарегистрированного пользователя
     * @throws UserAlreadyExistsException - пользователь с таким login уже зарегистрирован в системе
     */
    Optional<T> registerNewAccount(T t) throws UserAlreadyExistsException;


    Optional<T> registerNewClientAccount(T t, N n) throws UserAlreadyExistsException;

    /**
     * Список пользователей в роли courier
     * @return - {@link List<User>} у которых поле role имеет значение COURIER
     */
    List<T> couriersList();

}
