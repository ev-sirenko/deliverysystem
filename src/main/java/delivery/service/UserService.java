package delivery.service;

import delivery.dao.CustomerDAO;
import delivery.dao.UserDAO;
import delivery.exceptions.UserAlreadyExistsException;
import delivery.model.Customer;
import delivery.model.Role;
import delivery.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public class UserService implements IUserService<User, Customer>
{
    private final UserDAO userDAO;

    private final CustomerDAO customerDAO;

    @Autowired
    public UserService(UserDAO userDAO, CustomerDAO customerDAO)
    {
        this.userDAO = userDAO;
        this.customerDAO = customerDAO;
    }

    @Override
    @Transactional(rollbackFor = DataAccessException.class)
    public Optional<User> registerNewAccount(User user) throws UserAlreadyExistsException
    {
        if (userDAO.get(user.getLogin()).isPresent())
        {
            System.out.println("THROW:" + userDAO.get(user.getLogin()).get());
            throw new UserAlreadyExistsException("Пользователь с таким логином уже существует");
        }

        final Optional<Long> resultId = userDAO.save(user);

        if (resultId.isPresent())
        {
           return userDAO.get(resultId.get());
        }

        return Optional.empty();
    }

    @Override
    @Transactional(rollbackFor = {UserAlreadyExistsException.class, IllegalArgumentException.class})
    public Optional<User> registerNewClientAccount(User user, Customer customer) throws UserAlreadyExistsException
    {
        if (userDAO.get(user.getLogin()).isPresent())
        {
            System.out.println("THROW:" + userDAO.get(user.getLogin()).get());
            throw new UserAlreadyExistsException("Пользователь с таким логином уже существует");
        }

        final Optional<Long> resultId = userDAO.save(user);

        final Optional<Customer> optionalCustomer = customerDAO.saveCustomer(customer);

        if (resultId.isPresent())
        {
            return userDAO.get(resultId.get());
        }

        return Optional.empty();
    }

    @Override
    public List<User> couriersList()
    {
       return userDAO.getAll(Role.COURIER);
    }
}
