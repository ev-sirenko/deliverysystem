package delivery.service;

import delivery.exceptions.OrderNotFoundException;
import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface IOrderService<T>
{
    /**
     * Запрашивает запись по указанному id.
     * @param id - уникальный идентификатор записи
     * @return объект класса {@link Optional <T>}
     * @throws DataAccessException
     */
    T get(long id) throws OrderNotFoundException;


    /**
     * Запрашивает все записи
     * @return - список записей типа T
     * @throws DataAccessException
     */
    List<T> getAll(Map<String, String> parameters);

    /**
     * Сохраняет объект, создавая новую запись
     * @param t - объект. который нужно сохранить
     * @throws DataAccessException
     * @return - id вставленной записи
     */
    T save(T t);

    /**
     * Изменяет запись с @param id
     * @param t - объект, содержащий измененные поля
     * @throws DataAccessException
     */
    T update(T t);

    /**
     *
     * @param t
     * @return
     */
    T delete(T t);

    /**
     *
     * @return
     */
    List<T> getAll();
}
