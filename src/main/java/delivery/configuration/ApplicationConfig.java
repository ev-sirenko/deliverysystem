package delivery.configuration;


import delivery.dao.*;
import delivery.log.LogWriter;
import delivery.model.Customer;
import delivery.model.Order;
import delivery.model.User;
import delivery.service.IOrderService;
import delivery.service.IUserService;
import delivery.service.OrderService;
import delivery.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
@PropertySource({"classpath:application.properties"})
@Import(DataSourceConfig.class)
public class ApplicationConfig
{

    @Bean(name = "orderDAO")
    public OrderDAO personDao(JdbcTemplate template, LogWriter logWriter)
    {
        return new OrderDAO(template, logWriter);
    }

    @Bean(name = "statusHistoryDAO")
    public StatusHistoryDAO historyDAO(JdbcTemplate template)
    {
        return new StatusHistoryDAO(template);
    }


    @Bean(name = "customerDAO")
    public CustomerDAO customerDAO(JdbcTemplate template)
    {
        return new CustomerDAO(template);
    }

    @Bean(name = "userDAO")
    public UserDAO userDAO(JdbcTemplate template)
    {
        return new UserDAO(template);
    }

    @Bean(name = "deliveryDao")
    public DeliveryDao deliveryDao(JdbcTemplate template)
    {
        return new DeliveryDao(template);
    }

    @Bean
    public IUserService<User, Customer> userService(UserDAO userDAO, CustomerDAO customerDAO)
    {
        return new UserService(userDAO, customerDAO);
    }

    @Bean
    public IOrderService<Order> orderService(OrderDAO orderDAO)
    {
        return new OrderService(orderDAO);
    }

    @Bean
    public LogWriter logWriter()
    {
      return new LogWriter();
    }

}
