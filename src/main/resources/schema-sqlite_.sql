CREATE TABLE CUSTOMER (
	phone text PRIMARY KEY NOT NULL
);

CREATE TABLE ADDRESS (
	  id_address integer PRIMARY KEY NOT NULL
	, street text NOT NULL
	, building text NOT NULL
	, room text
	, building_floor integer
);

CREATE TABLE CUSTOMER_ADDRESS (
	  phone text NOT NULL
	, id_address NOT NULL
	, CONSTRAINT pk_customer_address PRIMARY KEY (phone, id_address)
	, FOREIGN KEY(phone) REFERENCES CUSTOMER(phone)
	, FOREIGN KEY (id_address) REFERENCES ADDRESS(id_address)
)

CREATE TABLE USER (
	id_user integer PRIMARY KEY,
	login text NOT NULL UNIQUE,
	password text NOT NULL,
	email text NOT NULL,
	user_role integer NOT NULL,
	FOREIGN KEY (user_role) REFERENCES USER_ROLE(code)
);

CREATE TABLE USER_ROLE (

	code integer PRIMARY KEY
	
)WITHOUT ROWID;

/*���� �������, ��� ��� integer �� ����� � ������� �����?*/
CREATE TABLE ITEM (
	id_item integer PRIMARY KEY,
	description text NOT NULL,
	price integer NOT NULL
)

CREATE TABLE STATUS (

	code integer PRIMARY KEY
	
)WITHOUT ROWID;

CREATE TABLE ORDERS (
	id_order integer PRIMARY KEY,
	phone text NOT NULL,
	status integer  NOT NULL,
	created_time text  NOT NULL,
	last_modified_time text  NOT NULL,
	start_date text NOT NULL,
	end_date text NOT NULL,
	what_time_to_deliver text,
	FOREIGN KEY(phone) REFERENCES CUSTOMER(phone),
	FOREIGN KEY (status) REFERENCES STATUS(code)
);

CREATE TABLE ORDER_ITEMS (
	  id_order integer NOT NULL
	, id_item integer NOT NULL
	, CONSTRAINT pk_order_items PRIMARY KEY (id_order, id_item)
	, FOREIGN KEY(id_order) REFERENCES ORDERS(id_order)
	, FOREIGN KEY (id_item) REFERENCES ITEM(id_item)
)


CREATE TABLE STATUS_HISTORY (
	id_status_history INTEGER NOT NULL,
	id_order INTEGER NOT NULL,
	status INTEGER NOT NULL, date_set TEXT NOT NULL,
	CONSTRAINT STATUS_HISTORY_PK PRIMARY KEY (id_status_history),
	CONSTRAINT STATUS_HISTORY_FK FOREIGN KEY (id_order) REFERENCES ORDERS(id_order),
	CONSTRAINT STATUS_HISTORY_FK_1 FOREIGN KEY (status) REFERENCES STATUS(code)
);

CREATE TABLE DELIVERY_SERVICE
(
	  id_order integer NOT NULL
	, id_user integer NOT NULL
	, start_date text
	, end_date text
	, CONSTRAINT pk_delivery_service PRIMARY KEY (id_order, id_user)
	, FOREIGN KEY(id_order) REFERENCES ORDERS(id_order)
	, FOREIGN KEY (id_user) REFERENCES CUSTOMER(phone)
)

CREATE INDEX ORDERS_phone_IDX ON ORDERS (phone);