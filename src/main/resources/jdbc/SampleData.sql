INSERT INTO CUSTOMER (phone) VALUES (89895662323), (89895662343);

INSERT INTO STATUS (code) VALUES (1),(2),(3),(4),(5);


INSERT INTO ORDERS (phone, status, created_time, last_modified_time, start_date, end_date, what_time_to_deliver)
VALUES (89895662323, 1, '2022-02-21 12:32:29', '2022-02-21 12:32:29', '2022-02-22', '2022-02-22', '15:00');
INSERT INTO ORDERS (phone, status, created_time,last_modified_time,start_date, end_date, what_time_to_deliver)
VALUES (89895662323, 3, '2022-02-21 13:45:29', '2022-02-21 15:31:40', '2022-02-22', '2022-02-22', '15:00');
INSERT INTO ORDERS (phone, status, created_time,last_modified_time,start_date, end_date, what_time_to_deliver)
VALUES (89895662343, 3, '2022-02-21 13:45:29', '2022-02-21 18:45:40', '2022-02-22', '2022-02-22', '15:00');


INSERT INTO STATUS_HISTORY (id_order, status, date_set)
VALUES(1, 1, '2022-02-21 12:32:29');
INSERT INTO STATUS_HISTORY (id_order, status, date_set)
VALUES(1, 2, '2022-02-21 12:45:25');
INSERT INTO STATUS_HISTORY (id_order, status, date_set)
VALUES(1, 2, '2022-02-21 12:45:25');

INSERT INTO USER_ROLE (code) VALUES(1), (2);

INSERT INTO USER
(login, password, email, user_role)
VALUES('Ivanov', '123', 'Ivanov@abyss.com', 1);

INSERT INTO USER
(login, password, email, user_role)
VALUES('Petrov', '1223', 'Petrov@abyss.com', 1);

INSERT INTO USER
(login, password, email, user_role)
VALUES('Sidorov', '1223', 'Sidorov@abyss.com', 1);


INSERT INTO ADDRESS (id_address, street, building, room, building_floor)
VALUES (1, 'Street', 4, 54, 4);

INSERT INTO CUSTOMER_ADDRESS (phone, id_address)
VALUES (89895662323, 1);

INSERT INTO ADDRESS (id_address, street, building, room, building_floor)
VALUES (2, 'Street2', 42, 542, 42);

INSERT INTO CUSTOMER_ADDRESS (phone, id_address)
VALUES (89895662343, 2);

INSERT INTO delivery_service
(id_order, id_user, start_date, end_date)
VALUES(1, 1, '2022-03-08 12:53:13', '2022-03-08 15:53:13');

INSERT INTO delivery_service
(id_order, id_user, start_date, end_date)
VALUES(3, 2, '2022-02-08 12:54:13', '2022-03-08 15:40:13');